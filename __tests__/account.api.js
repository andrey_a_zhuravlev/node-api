const request = require('supertest');
const app     = require('../index');
const User    = require('../models/User');

var token = "";

describe('Account api tests', () => {

  beforeAll(async (done) => {
    await User.destroy({ where: {email:"email@mail.com"}});
    done();
  });

  it('should create a new user', async (done) => {
    const res = await request(app)
      .post('/api/account.register?email=email@mail.com&password=pass&firstname=firstname&secondname=secondname')
      .send({});
    expect(res.status).toEqual(200);
    done();
  });

  it('should login user', async (done) => {
    const res = await request(app)
      .post('/api/account.login?email=email@mail.com&password=pass')
      .send({});
    
    expect(res.status).toEqual(200);
    expect(res.body.user).toHaveProperty('token');
    expect(res.body.user).toHaveProperty('email');
    expect(res.body.user).toHaveProperty('id');
    token = res.body.user.token;
    done();
  });

  it('should get user info', async (done) => {
    const res = await request(app)
      .get('/api/account.getInfo')
      .set('Authorization', token)
      .send({});
    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty('firstname');
    expect(res.body).toHaveProperty('secondname');
    expect(res.body).toHaveProperty('email');
    expect(res.body).toHaveProperty('id');
    expect(res.body).toHaveProperty('avatar');
    expect(res.body).toHaveProperty('lat');
    expect(res.body).toHaveProperty('lng');
    done();
  });

  it('should get error with wrong token', async (done) => {
    const res = await request(app)
      .get('/api/account.getInfo')
      .set('Authorization', 'wrong-token')
      .send({});
    expect(res.status).toEqual(401);
    done();
  });

})
