/*const request = require('supertest');
const router  = require('../routes/routes');
const auth    = require('../routes/auth');
const app = require('../index');
const db      = require('../config/sequelize');


var token = "";
*/
var httpMocks = require('node-mocks-http');
const account = require('../controllers/account');
const User    = require('../models/User');

const AccountControllerTestLogin = "accountctrl@mail.com";
var testUserId = 0;

function nextFunction(err) {
  console.log('ERROR Callback!!!', err)
}

describe('Account controller tests', () => {
  it('Create a new user', async (done) => {
    const request = httpMocks.createRequest({
      headers: {
        authentication: ''
      },
      query:{
        password: "password", 
        firstname: "firstname",
        secondname: "secondname",
        email: AccountControllerTestLogin
      }
    });
    var response = httpMocks.createResponse();
    const res = await account.register(request, response, nextFunction);
    expect(res.statusCode).toEqual(200);
    const { id } = JSON.parse(res._getData());
    testUserId = id;
    done();
  });
  
  it('Should not create a new user. Empty all fields.', async (done) => {
    const request = httpMocks.createRequest({});
    var response = httpMocks.createResponse();
    const res = await account.register(request, response, nextFunction);
    expect(res.statusCode).not.toEqual(200);
    done();
  });

  it('should not create a user with exist email(local)', async (done) => {
    const request = httpMocks.createRequest({
      headers: {
        authentication: ''
      },
      query:{
        password: "password", 
        firstname: "firstname",
        secondname: "secondname",
        email: AccountControllerTestLogin
      }
    });
    var response = httpMocks.createResponse();
    const res = await account.register(request, response, nextFunction);
    expect(res.statusCode).toEqual(422);
    done();
  });
  
  it('should not create a user without some fields (local)', async (done) => {
    const request = httpMocks.createRequest({
      headers: {
        authentication: ''
      },
      query:{
        password: "password", 
        secondname: "secondname",
        email: AccountControllerTestLogin
      }
    });
    var response = httpMocks.createResponse();
    const res = await account.register(request, response, nextFunction);
    expect(res.statusCode).toEqual(422);
    done();
  });

  it.skip('should login', async (done) => {
    const request = httpMocks.createRequest({
      method: 'POST',
      url: '/account.login?email=accountctrl@mail.com&password=password',
      headers: {
        authentication: ''
      },
      query:{
        password: "password", 
        email: AccountControllerTestLogin
      }
    });
    var response = httpMocks.createResponse();
    const res = await account.login(request, response, nextFunction);
    console.log(request);
    expect(res.statusCode).toEqual(200);
    done();
  });

  it('should getInfo', async (done) => {
    const request = httpMocks.createRequest({
      payload:{
        id: testUserId
      }
    });
    var response = httpMocks.createResponse();
    const res = await account.getInfo(request, response, nextFunction);
    expect(res.statusCode).toEqual(200);
    done();
  });

  it('should getInfo via query', async (done) => {
    const request = httpMocks.createRequest({
      query:{
        id: testUserId
      }
    });
    var response = httpMocks.createResponse();
    const res = await account.getInfo(request, response, nextFunction);
    expect(res.statusCode).toEqual(200);
    done();
  });

  it('should not getInfo', async (done) => {
    const request = httpMocks.createRequest({});
    var response = httpMocks.createResponse();
    const res = await account.getInfo(request, response, nextFunction);
    expect(res.statusCode).not.toEqual(200);
    done();
  });

  it('should updateInfo', async (done) => {
    const request = httpMocks.createRequest({
      payload:{
        id: testUserId
      },
      body:{
        firstname: "first",
        secondname: "second",
      }
    });
    var response = httpMocks.createResponse();
    const res = await account.updateInfo(request, response, nextFunction);
    expect(res.statusCode).toEqual(200);
    done();
  });

  it('should not updateInfo', async (done) => {
    const request = httpMocks.createRequest({});
    var response = httpMocks.createResponse();
    const res = await account.updateInfo(request, response, nextFunction);
    expect(res.statusCode).not.toEqual(200);
    done();
  });

  it('should setPosition', async (done) => {
    const request = httpMocks.createRequest({
      payload:{
        id: testUserId
      },
      body:{
        lat: "51.4234",
        lng: "24.24234",
      }
    });
    var response = httpMocks.createResponse();
    const res = await account.setPosition(request, response, nextFunction);
    expect(res.statusCode).toEqual(200);
    done();
  });

  it('should not setPosition', async (done) => {
    const request = httpMocks.createRequest({ });
    var response = httpMocks.createResponse();
    const res = await account.setPosition(request, response, nextFunction);
    expect(res.statusCode).not.toEqual(200);
    done();
  });

  afterAll(async (done) => {
    await User.destroy({ where: {email:AccountControllerTestLogin}});
    done();
  });
})