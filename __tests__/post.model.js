const User = require('../models/User');
const Post = require('../models/Post');
const db =  require('../config/sequelize');
const Sequelize = db.Sequelize;
const Op = Sequelize.Op;

const PostModelTestLogin = "posttestmodel@mail.com";

var testUserId = 0;
const testPostCount = 10;

describe('Post model tests', () => {

  it('Register user for test', async (done) => {
    const res = await User.registerLocalUser("andrey","zhuravlev", PostModelTestLogin, "test");
    expect(res).not.toBeNull();
    testUserId = res.id;
    done();
  });

  it('Add posts', async (done) => {
    for (let i=0; i<testPostCount; i++) {
        const res = await Post.add(testUserId, "Test header", "Test post body");
        expect(res).toBeTruthy();
    }
    done();
  });

  it('Get user posts', async (done) => {
    const postList = await Post.getPosts(testUserId);
    expect(postList).not.toBeNull();
    expect(postList).toHaveLength(testPostCount);
    done();
  });

  it('Change post', async (done) => {
    const postList = await Post.getPosts(testUserId);
    expect(postList).not.toBeNull();
    expect(postList).toHaveLength(testPostCount);
    const res = await Post.change(testUserId, postList[0].id, "New header", "New body");
    expect(res).toBeTruthy();
    done();
  });

  it('Delete all post', async (done) => {
    // Get all posts
    const postList = await Post.getPosts(testUserId);
    expect(postList).not.toBeNull();
    expect(postList).toHaveLength(testPostCount);
    // Delete all posts
    for (let i=0; i<testPostCount; i++) {
        const post = await Post.delete(testUserId, postList[i].id);
        expect(post).toBeTruthy();
    }
    // Check for delete
    const postListAfter = await Post.getPosts(testUserId);
    expect(postListAfter).not.toBeNull();
    expect(postListAfter).toHaveLength(0);
    done();
  });

  afterAll(async (done) => {
    await User.destroy({ where: {email:{[Op.substring]: PostModelTestLogin}}});
    done();
  });
})
