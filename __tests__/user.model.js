const User = require('../models/User');
const db =  require('../config/sequelize');
const Sequelize = db.Sequelize;
const Op = Sequelize.Op;
const UserModelTestLogin = "usertestmodel@mail.com";

var testUserId = 0;
const testUserCount = 20;

describe('User model tests', () => {

  it('Register many users', async (done) => {
    for (var i=0;i<testUserCount;i++) {
      var userEmail = UserModelTestLogin + "_"+i;
      const res = await User.registerLocalUser("andrey","zhuravlev", userEmail,"test");
      expect(res).not.toBeNull();
      testUserId = res.id;
    }
    done();
  });

  it('Get user list', async (done) => {
    const res = await User.getUserList(testUserId, "");
    expect(res).not.toBeNull();
    expect(res).toHaveLength(testUserCount);
    done();
  });
  
  it('Get user info', async (done) => {
    const res = await User.getInfo(testUserId);
    expect(res).not.toBeNull();
    done();
  });
  
  it('Update info', async (done) => {
    const newFirstname  = "New firstname";
    const newSecondname = "New secondname";
    const res = await User.updateInfo(testUserId, newFirstname, newSecondname);
    expect(res).toBeTruthy();
    const info = await User.getInfo(testUserId);
    expect(info.firstname).toBe(newFirstname);
    expect(info.secondname).toBe(newSecondname);
    done();
  });

  it('Set position', async (done) => {
    const lat = (Math.random()*100);
    const lng = (Math.random()*100);
    const res = await User.setPosition(testUserId, lat, lng);
    expect(res).toBeTruthy();
    const info = await User.getInfo(testUserId);
    expect(info.lat.toFixed(5)).toBe(lat.toFixed(5));
    expect(info.lng.toFixed(5)).toBe(lng.toFixed(5));
    done();
  });

  afterAll(async (done) => {
    await User.destroy({ where: {email:{[Op.substring]: UserModelTestLogin}}});
    done();
  });
})
