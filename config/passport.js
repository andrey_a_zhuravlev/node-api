const passport         = require('passport');
const LocalStrategy    = require('passport-local');
const FacebookTokenStrategy  = require('passport-facebook-token');
const User             = require('../models/User');
const {FacebookProviderName, LocalProviderName} = require('../providers');

const FACEBOOK_APP_ID = "517607039064422";
const FACEBOOK_APP_SECRET = "d330752db4c3f43f6c491344be01209b";

passport.use(new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
    }, async (email, password, done) => { 
      try{
        const user = await User.findOne({ where: { email: email, provider:LocalProviderName}});
        if(!user || !user.validatePassword(password)) {
          return done(null, false, { errors: { 'email or password': 'is invalid' } });
        }
        return done(null, user);
      } catch(error) {
        return done(error);
      }
    }));

passport.use(new FacebookTokenStrategy({
      clientID:     FACEBOOK_APP_ID,
      clientSecret: FACEBOOK_APP_SECRET
  },
    async (accessToken, refreshToken, profile, done) => {
      const email = profile.emails[0].value;
      try {
        const user = await User.findOne({ 
            attributes: ['firstname','secondname', 'email', 'avatar', 'id', 'lat', 'lng'],
            where: { email: email, provider: FacebookProviderName}});
        if (user) {
          return done(null, user);
        } else {
          const newUser = await User.registerFbUser(accessToken, profile);
          return done(null, newUser);
        }
      } catch(error) {
        return done(error);
      }
  }));