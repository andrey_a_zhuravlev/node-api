require('dotenv').config()
const Sequelize = require('sequelize');

const db = {};
const variants = {
    development:{
        url: process.env.DEV_DATABASE_URL,
        dialect: 'postgres',
    },
    test:{
        url: process.env.TEST_DATABASE_URL,
        dialect: 'postgres',
    },
    production:{
        url: process.env.DATABASE_URL,
        dialect: 'postgres',
    },
};
const env = process.env.NODE_ENV || 'development';
const sequelize = new Sequelize(variants[env].url);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;