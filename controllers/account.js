const passport = require('passport');
const User     = require('../models/User');
const Jimp     = require('jimp');

exports.login = function(request, response, next) {
    const email    = request.query.email;
    const password = request.query.password;
    if(!email) {
      return response.status(422).json({ errors: 'email is required',});
    }
    if(!password) {
      return response.status(422).json({ errors: 'password is required',});
    }
    return passport.authenticate('local', { session: false }, (err, passportUser, info) => {
      if(err) {
        return next(err);
      }
      
      if(passportUser) {
        const user = passportUser;
        user.token = passportUser.generateJWT();
        return response.json({ user: user.toAuthJSON() });
      }
      return response.status(400).send(info);
    })(request, response, next);
  };

exports.fblogin = function(request, response, next) {
    return passport.authenticate('facebook-token', {session: false}, (err, passportUser, info) => {
      if(err) {
        return next(err);
      }
      if(passportUser) {
        const user = passportUser;
        user.token = passportUser.generateJWT();
        return response.json({ user: user.toAuthJSON() });
      }
      return response.status(400).send(info);
    })(request, response, next);
  };
  
exports.register = async function(request, response, next) {
    const email      = request.query.email;
    const password   = request.query.password;
    const firstname  = request.query.firstname;
    const secondname = request.query.secondname;

    if (email==null || password==null || firstname==null || secondname==null) {
      return response.status(422).json("wrong arguments");
    }
    if (!email) {
      return response.status(422).json("email is required");
    }
    if (!password) {
      return response.status(422).json("password is required");
    }
    const result = await User.registerLocalUser(firstname,secondname,email,password);

    if (result) {
      return response.json(result);
    }

    return response.status(422).json("User create error!");
  };
exports.getInfo = async function(request, response, next) {
    let id = 0;
    if (request.payload!==undefined && request.payload.id!==undefined)
      id = request.payload.id;
    if (request.query!==undefined && request.query.id !== undefined) {
      if (!isNaN(parseInt(request.query.id)))
          id = request.query.id;
    }
    const user = await User.getInfo(id);
    if (user) {
      return response.json(user);
    }
    return response.status(422).json(request.payload);
};
exports.uploadAvatar = async function(request, response, next) {
    const id = request.payload.id;
    console.log(`Upload file done! File name ${request.file.filename}`);
    const file = await Jimp.read("public/images/"+request.file.filename);
    file.scaleToFit(64, 64).write("public/images/"+request.file.filename+"_small");

    const result = await User.updateAvatar(id, request.file.filename);
    if(request.file) {
      return response.json({avatar: request.file.filename});
    }
    return response.status(422).json("Could't save avatar!");
};
exports.updateInfo = async function(request, response, next) {
  if (request.body.firstname  != null && 
      request.body.secondname != null) {
        const res = await User.updateInfo(request.payload.id, 
                                      request.body.firstname, 
                                      request.body.secondname)
        if (res) {
            return response.status(200).json('done');
        }
    }
    return response.status(422).json("Request error");
};
exports.setPosition = async function(request, response, next) {
  if (request.body.lat != null && request.body.lng != null) {
      const res = await User.setPosition(request.payload.id, 
                                          request.body.lat, 
                                          request.body.lng)
      if (res) {
          return response.status(200).json('done');
      }
  }
  return response.status(422).json("Request error");
};