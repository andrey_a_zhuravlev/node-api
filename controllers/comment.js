const Comment = require('../models/Comment');

exports.add = async function(request, response, next) 
{
    if (request.body.postId != null && 
        request.body.body   != null) {
        const res = await Comment.add(request.payload.id, request.body.postId, request.body.body);
        if (res === true)
            return response.status(200).json('done');
    }
    return response.status(422).json("Request error");
}
