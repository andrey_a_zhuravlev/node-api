const User   = require('../models/User');
const Friend = require('../models/Friend');

function getRadian(x) {
  return x * Math.PI / 180;
};

function getDistance(p1_lat, p1_lng, p2_lat, p2_lng) {
  var R = 6378137; // Earth’s mean radius in meter
  var dLat = getRadian(p2_lat - p1_lat);
  var dLng = getRadian(p2_lng - p1_lng);
  var a = Math.pow(Math.sin(dLat / 2),2)+
          Math.cos(getRadian(p1_lat)) * Math.cos(getRadian(p2_lat)) *
          Math.pow(Math.sin(dLng / 2),2);
  var c = 2 * Math.asin(Math.sqrt(a));
  var d = R * c;
  
  return Math.round(d); // returns the distance in meter
};

// Friend search
exports.search = async function(request, response, next) {
    if (request.query.email==null) {
      return response.status(422).json({ errors: 'wrong arguments', });
    }
    const userList = await User.getUserList(request.payload.id, request.query.email);
    if (userList) {
      return response.json(userList);
    }
    return response.status(422).json({ errors: 'user not found!', });
};

// Get friendList
exports.getList = async function(request, response, next) {
  const res = await Friend.getFriends(request.payload.id);
  if (res) {
    return response.json(res);
  }
  return response.status(422).json("Request error");
};
// Add to friend (request)
exports.addFriend = async function(request, response, next) {
  if (request.query.id == null) {
    return response.status(422).json('wrong arguments');
  }
  console.log(`addFriend id = ${request.query.id}`);
  const res = await Friend.addFriend(request.payload.id, request.query.id);
  if (res) {
    return response.status(200).json('done');
  }
  return response.status(422).json("Request error");
};
exports.deleteFriend = async function(request, response, next) {
  if (request.query.id == null) {
    return response.status(422).json('wrong arguments');
  }
  const res = await Friend.deleteFriend(request.payload.id, request.query.id);
  if (res) {
    return response.status(200).json('done');
  }
  return response.status(422).json("Request error");
};

exports.getMapList = async function(request, response, next) {
  var radius = 10000;// default 10 km
  var list = [];
  const user = await User.findByPk(request.payload.id);
  const totalList = await Friend.getMapFriends(request.payload.id);

  if (request.query.radius != null)
    radius = request.query.radius;

  for (let i=0; i<totalList.length; i++) {
    if (totalList[i].lat && totalList[i].lng) {
      let dist = getDistance(user.lat, user.lng, totalList[i].lat, totalList[i].lng);
      console.log("user:", totalList[i].id,", distance = ", dist);
      if (dist < radius) {
        list.push(totalList[i]);
      }
    }
  }
  return response.json(list);
};