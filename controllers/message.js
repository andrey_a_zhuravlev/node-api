const Message = require('../models/Message');

exports.get = async function(request, response, next) 
{
    if (request.query.chatId  != null) {
        const result = await Message.get(request.query.chatId);
        if (result) {
            return response.json(result);
        }
    }
    return response.status(422).json("Request error");
}
