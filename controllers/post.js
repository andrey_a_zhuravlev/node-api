const Post = require('../models/Post');

exports.getPosts = async function(request, response, next) {
    let userId = request.payload.id;
    if (request.query.id) {
        if (!isNaN(parseInt(request.query.id)))
            userId = request.query.id;
    }
    const posts = await Post.getPosts(userId);
    if (posts) {
        return response.json(posts);
    }
    return response.status(422).json("Request error");
}
exports.add = async function(request, response, next) {
    if (request.body.header != null &&
        request.body.body   != null) {
        const res = await Post.add(request.payload.id, request.body.header, request.body.body);
        if (res) {
            return response.status(200).json('done');
        }
    }
    return response.status(422).json("Request error");
}
// POST
exports.change = async function(request, response, next) {
    if (request.body.postId != null && 
        request.body.header != null &&
        request.body.body   != null) {
        const res = await Post.change(request.payload.id, 
                                      request.body.postId, 
                                      request.body.header, 
                                      request.body.body)
        if (res) {
            return response.status(200).json('done');
        }
    }
    return response.status(422).json("Request error");
}
// POST 
exports.delete = async function(request, response, next) {
    if (request.query.postId != null) {
        const res = await Post.delete(request.payload.id, request.query.postId);
        if (res) {
            return response.status(200).json('done');
        }
    }
    return response.status(422).json("Rejected");
}
