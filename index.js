const express      = require('express');
const bodyParser   = require('body-parser');
const cors         = require('cors');
const path         = require('path');
const session      = require('express-session');
const errorHandler = require('errorhandler');
const Sequelize    = require('sequelize');

//Initiate our app
const app = express();

let port = process.env.PORT;
if (port == null || port == "") {
  port = 3001;
}

//Configure promise to global promise
Sequelize.Promise = global.Promise;

//var whitelist = ['https://glacial-earth-95962.herokuapp.com', 
//                 'https://nameless-ridge-54738.herokuapp.com']
var corsOptions = {
  origin: function (origin, callback) {
    callback(null, true);
    /*if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      console.log('Not allowed by CORS. Address:',origin);
    }*/
  }
}

//Configure our app
app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public/images')));
app.use(session({ secret: 'session-secret', 
                  cookie: { maxAge: 60000 }, 
                  resave: false, 
                  saveUninitialized: false }));

app.use(errorHandler());

require('./models/User');
require('./config/passport');
app.use(require('./routes'));

//Error handlers & middlewares
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    errors: {
      message: err.message,
      error: err,
    },
  });
});


app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    errors: {
      message: err.message,
      error: {},
    },
  });
});

const server = app.listen(port, () => {
  console.log(`App running on port ${port}.`)
});
// Start chat server
const io      = require('socket.io')(server, { origins: '*:*'});
const Message = require('./models/Message');

io.on('connection', function(client) {
    console.log('a user connected', client.id);
    
    client.on('join', function(chatId){
        console.log('join: ', chatId);
        client.join(chatId);
    });
    client.on('message', function(data){
        console.log('message: ', data);
        Message.add(data.chatId, 
                    data.fromId, 
                    data.toId, 
                    data.message);
        io.sockets.to(data.chatId).emit('message', data);
    });
    client.on('disconnect', function(){
        console.log('user disconnected', client.id);
    });
    client.on('error', function (err) {
        console.log('received error from client:', client.id)
        console.log(err)
    })
});

module.exports = app;