const User    = require('./models/User');
const Friend  = require('./models/Friend');
const Comment = require('./models/Comment');
const Post    = require('./models/Post');
const Message = require('./models/Message');

beforeAll( async (done) => {
    await User.sync();
    await Friend.sync();
    await Comment.sync();
    await Post.sync();
    await Message.sync();
    done();
});