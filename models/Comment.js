const db =  require('../config/sequelize');

const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

const Model = db.Sequelize.Model;
class Comment extends Model { }
module.exports = Comment;
//Comment.sync();

const Post = require('../models/Post');

Comment.add = async (userId, postId, body) => {
    try {
        const post    = await Post.findByPk(postId);
        const comment = await Comment.create({ body: body, 
                                               userId: userId});
        await post.addComment(comment);
        return true;
    } catch(error) {
        console.log(error.message);
        return false;
    }
}

Comment.init({
    body: Sequelize.STRING(1024)
}, {sequelize, modelName: 'comment'});

//sequelize.sync();