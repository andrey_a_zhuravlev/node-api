const db =  require('../config/sequelize');
const nodemailer = require('nodemailer');

const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

const Op = Sequelize.Op;
const Model = Sequelize.Model;

class Friend extends Model {}
module.exports = Friend;
//Friend.sync();
const User = require('../models/User');

const gmail = "scnettwork@gmail.com";
const mailtransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: gmail,
    pass: 'reptile13?'
  }
});

Friend.getFriends = async (id) => {
  try {
    const user = await User.findByPk(id);
    const attributes = { 
            attributes: ['id','firstname','secondname', 'email', 'avatar'], 
            joinTableAttributes: ['accepted', 'FirstId', 'SecondId']};
    let friendList1 = await user.getFirsts(attributes);
    let friendList2 = await user.getSeconds(attributes);
    let friendList  = friendList1.concat(friendList2);
    console.log(`getFriends friendList = ${JSON.stringify(friendList)}`);
    return friendList;
  } catch(error) {
    console.log(error.message);
    return null;
  }
}
Friend.getMapFriends = async (id) => {
  try {
    const user = await User.findByPk(id);
    const attributes = { 
            attributes: ['id','firstname','secondname', 'email', 'avatar', 'lat', 'lng'], 
            joinTableAttributes: ['accepted', 'FirstId', 'SecondId']};
    let friendList1 = await user.getFirsts(attributes);
    let friendList2 = await user.getSeconds(attributes);
    let friendList  = friendList1.concat(friendList2);
    console.log(`getFriends friendList = ${JSON.stringify(friendList)}`);
    return friendList;
  } catch(error) {
    console.log(error.message);
    return null;
  }
}
Friend.addFriend = async (userId, friendId) => {
  try {
    const user = await User.findByPk(userId);
    const friend = await User.findByPk(friendId);
    const friendship = await Friend.findOne({where: {[Op.or]:[
                        {FirstId: userId,   SecondId: friendId},
                        {FirstId: friendId, SecondId: userId}]}});
    console.log(`addFriend userId=${userId} friendship = ${JSON.stringify(friendship)}`);
    if (friendship == null) {
      await user.addFirst(friendId);
      // send mail here
      let mailText = "Пользователь " + user.firstname + " " +
                                       user.secondname + " (" +
                                       user.email + ") " + 
                      "приглашает вас дружить!";
      var mailOptions = {
        from: gmail,
        to: friend.email,
        subject: "Social network: Приглашение дружить",
        text: mailText
      };
      mailtransport.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      }); 
      // send mail here
      return true;
    }
    if(friendship.FirstId != userId && friendship.accepted == false) {
      await Friend.update({accepted: true},{where: { FirstId: friendId,  SecondId:userId}});
      return true;
    }
  } catch(error) {
    console.log(error.message);
    return false;
  }
}
Friend.deleteFriend = async (userId, friendId) => {
  try {
    const user = await User.findByPk(userId);
    const attributes = { 
            attributes: ['id'], 
            joinTableAttributes: ['accepted', 'FirstId', 'SecondId'],
            where:{ [Op.or]:[{'$friend.FirstId$': friendId},
                             {'$friend.SecondId$': friendId}] }
          };
    const firstIdList  = await user.getFirsts(attributes);
    const secondIdList = await user.getSeconds(attributes);
    if (firstIdList.length != 0 || secondIdList.length != 0) {
      await Friend.destroy({
        where: {[Op.or]:[{FirstId: userId,   SecondId: friendId},
                         {FirstId: friendId, SecondId: userId}]}
      });
      return true;
    }
  } catch(error) {
    console.log(error.message);
    return false;
  }
  return false;
}

Friend.init({
  accepted: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  }
}, {sequelize, modelName: 'friend'});
//sequelize.sync();
