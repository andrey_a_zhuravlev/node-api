const db =  require('../config/sequelize');
const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

const Model = Sequelize.Model;
class Message extends Model { }
module.exports = Message;
//Message.sync();

Message.add = async (chatId, fromId, toId, message) => {
    try {
        await Message.create({ chatId:  chatId, 
                               fromId:  fromId,
                               toId:    toId,
                               message: message});
        return true;
    } catch(error) {
        console.log(error.message);
        return false;
    }
}
Message.get = async (chatId) => {
    try {
        const messages = await Message.findAll({
            attributes: ['chatId','fromId','toId', 'message'], 
            where: {chatId: chatId}, 
            limit: 20,
            });
        return messages;
    } catch(error) {
        console.log(error.message);
        return null;
    }
    return null;
}

Message.init({
    chatId:  Sequelize.STRING,
    fromId:  Sequelize.STRING,
    toId:    Sequelize.STRING,
    message: Sequelize.STRING(1024)
}, {sequelize, modelName: 'message'});

//sequelize.sync();