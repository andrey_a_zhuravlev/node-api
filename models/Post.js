const db =  require('../config/sequelize');

const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

const Model = Sequelize.Model;

class Post extends Model { }
module.exports = Post;
//Post.sync();

const User    = require('../models/User');
const Comment = require('../models/Comment');

Post.getPosts = async (userId) => {
    try {
        const user = await User.findByPk(userId);
        const attributes = { 
            attributes: ['id', 'header', 'body', 'userId', 'createdAt'], 
            order: [['createdAt', 'DESC'],
                    [Comment, 'createdAt', 'ASC']],
            include: {
                model:Comment,
                attributes: ['body', 'postId', 'userId', 'createdAt'],
                include: {
                    model:User,
                    attributes: ['firstname', 'secondname', 'avatar', 'id']
                }
            }
        };
        const posts = await user.getPosts(attributes);
        return posts;
    } catch(error) {
        console.log(error.message);
        return null;
    }
}
Post.add = async (userId, header, body) => {
    try {
        const user = await User.findByPk(userId);
        const post = await Post.create({header: header, 
                                        body:   body});
        await user.addPost(post);
        return true;
    } catch(error) {
        console.log(error.message);
        return false;
    }
}
Post.change = async (userId, postId, header, body) => {
    try {
        const post = await Post.findByPk(postId);
        if (post.userId == userId) {
            post.header = header;
            post.body   = body;
            await post.save();
            return true;
        }
        return true;
    } catch(error) {
        console.log(error.message);
    }
    return false;
}
Post.delete = async (userId, postId) => {
    try {
        const user = await User.findByPk(userId);
        const post = await Post.findByPk(postId);
        //console.log(`delete post.userId=${post.userId} userId=${userId}`);
        if (post.userId == userId) {
            await user.removePost(post);
            await post.destroy();
            return true;
        }
    } catch(error) {
        console.log(error.message);
    }
    return false;
}

Post.init({
    header: Sequelize.STRING,
    body: Sequelize.STRING(1024)
}, {sequelize, modelName: 'post'});

//sequelize.sync();

