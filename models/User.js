const crypto    = require('crypto');
const jwt       = require('jsonwebtoken');
const {FacebookProviderName, LocalProviderName} = require('../providers');

const db =  require('../config/sequelize');

const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

const Op = Sequelize.Op;
const Model = Sequelize.Model;

class User extends Model {
  setPassword = (password) => {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  };
  
  validatePassword = (password) => {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
  };
  
  generateJWT = () => {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);
  
    return jwt.sign({
      email: this.email,
      id: this.id,
      exp: parseInt(expirationDate.getTime() / 1000, 10),
    }, 'secret');
  }
  toAuthJSON = () => {
    return {
      id: this.id,
      email: this.email,
      token: this.generateJWT(),
    };
  };
  setName = (firstname, secondname) => {
    this.firstname = firstname;
    this.secondname = secondname;
  };
  setEmail = (email) => {
    this.email = email;
  }
  setProvider = (provider) => {
    this.provider = provider;
  }
}
module.exports = User;
//User.sync();

const Friend  = require('./Friend');
const Comment = require('./Comment');
const Post    = require('./Post');

User.getUserList = async (id, email) => {
  try {
    const userList = await User.findAll({
        attributes: ['id','firstname','secondname', 'email', 'avatar'], 
        where: { email: {[Op.substring]: email},
                 id: {[Op.ne]: id}}, 
        limit: 20,
        include: [{ 
          model: User,
          as: 'Firsts',
          attributes: ['id'],
          through: {
            model: Friend,
            attributes: ['accepted'],
            where: {[Op.or]:[{FirstId: id}, {SecondId: id}]}
          }
          },
          { model: User,
            as: 'Seconds',
            attributes: ['id'],
            through: {
              model: Friend,
              attributes: ['accepted'],
              where: {[Op.or]:[{FirstId: id}, {SecondId: id}]}
            }
            }]
        });
    return userList;
  } catch(error) {
    console.log(error.message);
    return null;
  }
}
User.getInfo = async (id) => {
  try {
    const user = await User.findOne( {
                  attributes: ['firstname','secondname', 'email', 'avatar', 'id', 'lat', 'lng'], 
                  where: { id: id } });
    return user;
  } catch(error) {
    console.log(error.message);
    return null;
  }
}
User.getUser = async (email, provider) => {
  try {
    const user = await User.findOne({ 
                    attributes: ['id'], 
                    where: { email: email, 
                            provider:provider }});
    return user;
  } catch(error) {
    console.log(error.message);
    return null;
  }
}
User.updateAvatar = async (id,filename) => {
  try {
    await User.update({avatar: filename},
                      {where: { id: id }});
    return true;
  } catch(error) {
    console.log(error.message);
    return false;
  }
}
User.updateInfo = async (id,firstname,secondname) => {
  try {
    const user = await User.findByPk(id);
    user.firstname  = firstname;
    user.secondname = secondname;
    await user.save();
    return true;
  } catch(error) {
    console.log(error.message);
    return false;
  }
}
User.setPosition = async (id,lat,lng) => {
  try {
    const user = await User.findByPk(id);
    user.lat = lat;
    user.lng = lng;
    await user.save();
    return true;
  } catch(error) {
    console.log(error.message);
    return false;
  }
}
User.registerLocalUser = async (firstname, secondname, email, password) => {
  try {
    const userIsExist = await User.getUser(email, LocalProviderName);
    if (userIsExist) {
      return null;
    }
    const newUser = new User();
    newUser.setPassword(password);
    newUser.setName(firstname, secondname);
    newUser.setEmail(email);
    newUser.setProvider(LocalProviderName);
    await newUser.save();
    return newUser.toAuthJSON();
  } catch(error) {
    console.log(error.message);
    return null;
  }
}
User.registerFbUser = async (accessToken, profile) => {
  try {
    const newUser = new User();
    
    newUser.setName(profile.name.givenName, profile.name.familyName);
    newUser.setEmail(profile.emails[0].value);
    newUser.setProvider(FacebookProviderName);
    await newUser.save();
    return newUser;
  } catch(error) {
    console.log(error.message);
    return null;
  }
}
User.init({
    firstname:  Sequelize.STRING,
    secondname: Sequelize.STRING,
    email:      Sequelize.STRING,
    hash:       Sequelize.STRING(1024),
    salt:       Sequelize.STRING,
    avatar:     Sequelize.STRING,
    provider:   Sequelize.STRING,
    lat:        Sequelize.DOUBLE,
    lng:        Sequelize.DOUBLE
}, {
  sequelize,
  modelName: 'user'
  // options
});

User.belongsToMany(User, { through: Friend, as: 'Firsts',  foreignKey: 'FirstId' });
User.belongsToMany(User, { through: Friend, as: 'Seconds', foreignKey: 'SecondId' });

Friend.belongsTo(User, { as: 'First'});
Friend.belongsTo(User, { as: 'Second'});

User.hasMany(Post);
Post.belongsTo(User);

Post.hasMany(Comment, { onDelete: 'cascade', hooks: true });
Comment.belongsTo(Post);

User.hasMany(Comment);
Comment.belongsTo(User);

//sequelize.sync();
