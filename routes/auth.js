const jwt = require('express-jwt');

const getTokenFromHeaders = (req) => {
  
  const { headers: { authorization } } = req;
  if(authorization) { 
    return authorization;
  }
  return null;
};

const auth = {
  private: jwt({
    secret: 'secret',
    userProperty: 'payload',
    getToken: getTokenFromHeaders,
  }),
  public: jwt({
    secret: 'secret',
    userProperty: 'payload',
    credentialsRequired: false,
  }),
};

module.exports = auth;