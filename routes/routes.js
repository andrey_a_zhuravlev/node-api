const router  = require('express').Router();
const auth    = require('./auth');
const account = require('../controllers/account');
const friends = require('../controllers/friends');
const post    = require('../controllers/post');
const comment = require('../controllers/comment');
const message = require('../controllers/message');
const multer  = require('multer');
const upload  = multer({dest: 'public/images'});
const db =  require('../config/sequelize');
db.sequelize.sync();

const objectList = {
    account: account,
    friends: friends,
    post:    post,
    comment: comment,
    message: message
};

function getFuncFromRequest(request) {
    const path = request.path.replace('/','').trim();
    const objectStr = path.slice(0, path.indexOf('.'));
    const functionStr = path.slice(path.indexOf('.')+1);
    //console.log(`objectStr = ${objectStr}; functionStr = ${functionStr}`);
    const objectName = objectList[objectStr];
    if (objectName == null) {
        console.log(`objectName == null`);
        return null;
    }
    const fn = objectName[functionStr];
    if (typeof fn === "function") {
        return fn;
    }
    return null;
}
function routeFunction(request, response, next){
    var fn = getFuncFromRequest(request);
    if (fn == null)
        return response.status(400).json({ errors: 'function not found', });
    return fn(request, response, next);
}
//POST register new user route (public, everyone has access)
router.post('/account.register', auth.public, account.register);

//POST login route (public, everyone has access)
router.post('/account.login', auth.public, account.login);

router.post('/account.fblogin', auth.public, account.fblogin);

//POST route for upload avatar (private, only authenticated users have access)
router.post('/account.uploadAvatar', auth.private, upload.single('avatar'), account.uploadAvatar);

//POST route for all requests (private, only authenticated users have access)
router.post('/*', auth.private, routeFunction);

//GET route for all requests (private, only authenticated users have access)
router.get('/*', auth.private, routeFunction);

module.exports = router;